<!DOCTYPE html>
<html>
<head>
	<title>Halaman Form</title>
</head>
<body>


<h1>Buat Account Baru!</h1>

<h3>Sign Up Form</h3>

<form action="/welcome" method="POST">
	@csrf
  <p>
    <label for="fname">First name:</label>
  </p>
    <input type="text" id="fname" name="fname"><br>

  <p>
    <label for="lname">Last name:</label><br>
  </p>
    <input type="text" id="lname" name="lname">

    
  <p>Gender:</p>
    <input type="radio" id="male" name="gender" value="male">
     <label for="male">Male</label><br>

    <input type="radio" id="female" name="gender" value="female">
      <label for="female">Female</label><br>

    <input type="radio" id="other" name="gender" value="other">
      <label for="other">Other</label>

  <p>
    <label>Nationality:</label>
      <p>
        <select>
          <option value="Indonesia">Indonesia</option>
          <option value="Singapore">Singapore</option>
          <option value="Malaysia">Malaysia</option>
          <option value="Australia">Australia</option>
        </select>

  <p>Language Spoken:</p>
    <input type="checkbox" id="ind" name="Spoken" value="indonesia">
      <label for="indonesia">Bahasa Indonesia</label>
        <br>
      

    <input type="checkbox" id="eng" name="Spoken" value="english">
      <label for="english">English</label>
        <br>

    <input type="checkbox" id="oth" name="Spoken" value="Other">
      <label for="other">Other</label>
        <br>

 

  <p>
    <label for="alamat">Bio:</label>
  </p>
    <textarea cols="30" rows="10"></textarea>

  <br>
    <input type="submit" name="submit" value="Sign Up">

</form>
</body>
</html>